# BetterCode前端应用

## 第三方应用登陆

BetterCode前端应用部署前，需要将[login.vue](bettercode-frontend/src/components/login/login.vue)中处理各个第三方平台的application id替换掉。

> 如果出现修改application id后页面跳转不正确的情况，清楚浏览器缓存后重试