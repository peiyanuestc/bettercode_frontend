import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
      path: '/',
      redirect: '/login'
    },
    {
      path: '/',
      component: resolve => require(['../components/home.vue'], resolve),
      meta: {
        title: 'home'
      },
      children: [{
        path: 'dashboard/:platform/:user/:owner/:name',
        component: resolve => require(['../components/Dashboard.vue'], resolve),
        meta: {
          title: '控制面板'
        },
      }, {
        path: 'issues/:platform/:user/:owner/:name',
        component: resolve => require(['../components/issues.vue'], resolve),
        meta: {
          title: 'issues'
        }
      }, {
        path: 'life/:platform/:user/:owner/:name',
        component: resolve => require(['../components/lifecircle.vue'], resolve),
        meta: {
          title: 'lifecircle'
        }
      }, {
        path: 'overview/:platform/:user',
        component: resolve => require(['../components/Overview.vue'], resolve),
        name: 'overview',
        meta: {
          title: 'Overview'
        }
      }, {
        path: '404',
        component: resolve => require(['../components/commons/404.vue'], resolve),
        meta: {
          title: '404'
        }
      }]
    },
    // {
    //   path: '/issues',
    //   component: resolve => require(['../components/issues.vue'], resolve)
    // },
    // {
    //   path: '/life',
    //   component: resolve => require(['../components/lifecircle.vue'], resolve)
    // },
    {
      path: '/login',
      component: resolve => require(['../components/login/login.vue'], resolve)
    },{
      path: '/github',
      component: resolve => require(['../components/login/socialin.vue'], resolve)
    },{
      path: '/gitlab',
      component: resolve => require(['../components/login/socialin.vue'], resolve)
    },{
      path: '/seciii',
      component: resolve => require(['../components/login/socialin.vue'], resolve)
    },
    {
      path: '/rela',
      component: resolve => require(['../components/study-components/hh/realation.vue'], resolve)
    },
    {
      path: '/relaitem',
      component: resolve => require(['../components/study-components/hh/relation-items.vue'], resolve)
    },
    {
      path: '/relasearch',
      component: resolve => require(['../components/study-components/hh/relation-search.vue'], resolve)
    },
    {
      path: '/entity',
      component: resolve => require(['../components/study-components/hh/entityrec.vue'], resolve)
    },
    {
      path: '*',
      redirect: '/404'
    },
  ]
})
